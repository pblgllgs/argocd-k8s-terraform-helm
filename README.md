# ARGOCD-TERRAFORM-K8S-HELM-DEPLOY

USE K8S FROM DOCKER DESKTOP

## TERRAFORM

```BASH
terraform init
```

### APPLY

```BASH
terraform apply --auto-approve
```

## DEPLOY K8S MANIFESTS

```BASH
kubectl create namespace dev
kubectl apply -f k8s/
```

## SEE DASHBOARD ARGOCD

```BASH
kubectl port-forward svc/argocd-server -n argocd 8080:80
```

## USERNAME: "admin", GET PASSWORD

```BASH
argocd admin initial-password -n argocd
```
